//
//  SettingsViewController.swift
//  SilverCoinValuerPRO
//
//  Created by Dave Murphy on 06/10/2016.
//  Copyright © 2016-17 www.silvercoinvaluer.com. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    static let selectedList = "selected_list"
    static let selectedCurrency = "selected_currency"
    static let selectedSearch = "selected_search"
    static let usdPrice = "usd_price"
    static let eurPrice = "eur_price"
    static let gbpPrice = "gbp_price"
    static let cadPrice = "cad_price"
    
    @IBOutlet weak var listPicker: UIPickerView!
    @IBOutlet weak var currencyPicker: UIPickerView!
    @IBOutlet weak var searchPicker: UIPickerView!
    
    var lists = ["All coins", "Popular coins", "ASW >2oz", "Bookmarks"]
    var currencies = ["USD", "EUR", "GBP", "CAD"]
    var searches = ["By country", "By name"]
    
    // use closure to instantiate the number formatter
    let numberFormatter: NumberFormatter = {
        let nf = NumberFormatter()
        nf.numberStyle = .decimal
        nf.minimumFractionDigits = 2
        nf.maximumFractionDigits = 2
        return nf
    }()
    
    
    @IBOutlet weak var priceLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = ""
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.listPicker.delegate = self
        self.currencyPicker.delegate = self
        self.searchPicker.delegate = self
        
        self.listPicker.dataSource = self
        self.currencyPicker.dataSource = self
        self.searchPicker.dataSource = self
        
        let prefs = UserDefaults.standard
        
        // Set initial priceLabel text
        if(prefs.integer(forKey: SettingsViewController.selectedCurrency) == 0) {
            priceLabel.text = numberFormatter.string(from: NSNumber(value: prefs.double(forKey: SettingsViewController.usdPrice)))
        } else if (prefs.integer(forKey: SettingsViewController.selectedCurrency) == 1) {
            priceLabel.text = numberFormatter.string(from: NSNumber(value: prefs.double(forKey: SettingsViewController.eurPrice)))
        } else if (prefs.integer(forKey: SettingsViewController.selectedCurrency) == 2) {
            priceLabel.text = numberFormatter.string(from: NSNumber(value: prefs.double(forKey: SettingsViewController.gbpPrice)))
        } else if (prefs.integer(forKey: SettingsViewController.selectedCurrency) == 3) {
            priceLabel.text = numberFormatter.string(from: NSNumber(value: prefs.double(forKey: SettingsViewController.cadPrice)))
        }
        
        // Set initial picker values
        listPicker.selectRow(prefs.integer(forKey: SettingsViewController.selectedList), inComponent: 0, animated: true)
        currencyPicker.selectRow(prefs.integer(forKey: SettingsViewController.selectedCurrency), inComponent: 0, animated: true)
        searchPicker.selectRow(prefs.integer(forKey: SettingsViewController.selectedSearch), inComponent: 0, animated: true)
    }
    
    
    @IBAction func requestButton(_ sender: UIButton) {
        let request = URLRequest(url: URL(string: "http://www.goldandsilvertax.com/precious-metals.php")!)
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) -> Void in
            
            if let jsonData = data {
                //                if let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) {
                //                    print(jsonString)
                //                }
                do {
                    let jsonObject: AnyObject = try JSONSerialization.jsonObject(with: jsonData, options: []) as! [String: Any] as AnyObject
                    //print(jsonObject)
                    let prefs = UserDefaults.standard
                    
                    if let usd = jsonObject["usd"] as? [String: Double] {
                        if var silverPrice = usd["silver"] {
                            silverPrice = silverPrice * 31.1
                            prefs.set(silverPrice, forKey: SettingsViewController.usdPrice)
                            print("Silver in USD: \(prefs.double(forKey: SettingsViewController.usdPrice))")
                        }
                    }
                    
                    if let eur = jsonObject["eur"] as? [String: Double] {
                        if var silverPrice = eur["silver"] {
                            silverPrice = silverPrice * 31.1
                            prefs.set(silverPrice, forKey: SettingsViewController.eurPrice)
                            print("Silver in EUR: \(prefs.double(forKey: SettingsViewController.eurPrice))")
                        }
                    }
                    
                    if let gbp = jsonObject["gbp"] as? [String: Double] {
                        if var silverPrice = gbp["silver"] {
                            silverPrice = silverPrice * 31.1
                            prefs.set(silverPrice, forKey: SettingsViewController.gbpPrice)
                            print("Silver in GBP: \(prefs.double(forKey: SettingsViewController.gbpPrice))")
                        }
                    }
                    
                    if let cad = jsonObject["cad"] as? [String: Double] {
                        if var silverPrice = cad["silver"] {
                            silverPrice = silverPrice * 31.1
                            prefs.set(silverPrice, forKey: SettingsViewController.cadPrice)
                            print("Silver in CAD: \(prefs.double(forKey: SettingsViewController.cadPrice))")
                        }
                    }
                    DispatchQueue.main.async {
                        self.reloadPriceLabel()
                    }
                }
                catch let error {
                    print("Error creating JSON object: \(error)")
                }
            }
            else if let requestError = error {
                print("Error fetching silver price data: \(requestError)")
            }
            else {
                print("Unexpected error with the request")
            }
        })
        task.resume()
    }
    
    
    func reloadPriceLabel() {
        print("reloadPriceLable() method called")
        let prefs = UserDefaults.standard
        if(prefs.integer(forKey: SettingsViewController.selectedCurrency) == 0) {
            priceLabel.text = numberFormatter.string(from: NSNumber(value: prefs.double(forKey: SettingsViewController.usdPrice)))
        } else if (prefs.integer(forKey: SettingsViewController.selectedCurrency) == 1) {
            priceLabel?.text = numberFormatter.string(from: NSNumber(value: prefs.double(forKey: SettingsViewController.eurPrice)))
        } else if (prefs.integer(forKey: SettingsViewController.selectedCurrency) == 2) {
            priceLabel?.text = numberFormatter.string(from: NSNumber(value: prefs.double(forKey: SettingsViewController.gbpPrice)))
        } else if (prefs.integer(forKey: SettingsViewController.selectedCurrency) == 3) {
            priceLabel?.text = numberFormatter.string(from: NSNumber(value: prefs.double(forKey: SettingsViewController.cadPrice)))
        }
    }

    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == listPicker {
            return lists.count
        } else if pickerView == currencyPicker {
            return currencies.count
        } else if pickerView == searchPicker {
            return searches.count
        }
        return 1
    }

    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        if pickerView == listPicker {
            let pickerLabel = UILabel()
            pickerLabel.textColor = UIColor.black
            pickerLabel.text = lists[row]
            pickerLabel.font = UIFont(name: pickerLabel.font.fontName, size: 12)
            //pickerLabel.font = UIFont(name: "Arial-BoldMT", size: 15) // In this use your custom font
            pickerLabel.textAlignment = NSTextAlignment.center
            
            // if no bookmarks, grey out Bookmarks text
            let prefs = UserDefaults.standard
            
            // check if there's zero bookmarks or not
            let anyBookmarks = prefs.object(forKey: DBItemsViewController.noBookmarks) as? Bool
            
            if anyBookmarks != nil {
                if anyBookmarks != true {
                    if pickerLabel.text == "Bookmarks" {
                        pickerLabel.textColor = UIColor.lightGray
                    }
                }
            }
            
            return pickerLabel
        } else if pickerView == currencyPicker {
            let pickerLabel = UILabel()
            pickerLabel.textColor = UIColor.black
            pickerLabel.text = currencies[row]
            pickerLabel.font = UIFont(name: pickerLabel.font.fontName, size: 12)
            //pickerLabel.font = UIFont(name: "Arial-BoldMT", size: 15) // In this use your custom font
            pickerLabel.textAlignment = NSTextAlignment.center
            return pickerLabel
        } else if pickerView == searchPicker {
            let pickerLabel = UILabel()
            pickerLabel.textColor = UIColor.black
            pickerLabel.text = searches[row]
            pickerLabel.font = UIFont(name: pickerLabel.font.fontName, size: 12)
            //pickerLabel.font = UIFont(name: "Arial-BoldMT", size: 15) // In this use your custom font
            pickerLabel.textAlignment = NSTextAlignment.center
            return pickerLabel
        }
        return listPicker
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let prefs = UserDefaults.standard
        
        // set the various NSUserDefaults depending on which row item selected
        // currencyPicker also updates the text on priceLabel
        if pickerView == listPicker {
            if(lists[row] == "All coins") {
                prefs.set(0, forKey: SettingsViewController.selectedList)
            } else if (lists[row] == "Popular coins") {
                prefs.set(1, forKey: SettingsViewController.selectedList)
            } else if (lists[row] == "ASW >2oz") {
                prefs.set(2, forKey: SettingsViewController.selectedList)
            } else if (lists[row] == "Bookmarks") {
                // if no bookmarks, selecting Bookmarks defaults to All Coins
                let prefs = UserDefaults.standard
                
                // check if there's zero bookmarks or not
                let anyBookmarks = prefs.object(forKey: DBItemsViewController.noBookmarks) as? Bool
                
                if anyBookmarks != nil {
                    if anyBookmarks == true {
                        print("yes there are bookmarks")
                        prefs.set(3, forKey: SettingsViewController.selectedList)
                    } else {
                        print("nope, no bookmarks")
                        prefs.set(0, forKey: SettingsViewController.selectedList)
                    }
                }
            }
        } else if pickerView == currencyPicker {
            if(currencies[row] == "USD") {
                priceLabel.text = numberFormatter.string(from: NSNumber(value: prefs.double(forKey: SettingsViewController.usdPrice)))
                prefs.set(0, forKey: SettingsViewController.selectedCurrency)
            } else if (currencies[row] == "EUR") {
                priceLabel.text = numberFormatter.string(from: NSNumber(value: prefs.double(forKey: SettingsViewController.eurPrice)))
                prefs.set(1, forKey: SettingsViewController.selectedCurrency)
            } else if (currencies[row] == "GBP") {
                priceLabel.text = numberFormatter.string(from: NSNumber(value: prefs.double(forKey: SettingsViewController.gbpPrice)))
                prefs.set(2, forKey: SettingsViewController.selectedCurrency)
            } else if (currencies[row] == "CAD") {
                priceLabel.text = numberFormatter.string(from: NSNumber(value: prefs.double(forKey: SettingsViewController.cadPrice)))
                prefs.set(3, forKey: SettingsViewController.selectedCurrency)
            }
        } else if pickerView == searchPicker {
            if(searches[row] == "By country") {
                prefs.set(0, forKey: SettingsViewController.selectedSearch)
            } else if(searches[row] == "By name") {
                prefs.set(1, forKey: SettingsViewController.selectedSearch)
            }
        }
        print("")
    }
    
    
    func uicolorFromHex(rgbValue:UInt32)->UIColor{
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        
        return UIColor(red:red, green:green, blue:blue, alpha:1.0)
    }
}
