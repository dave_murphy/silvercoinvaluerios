//
//  DBItem.swift
//  SilverCoinValuerPRO
//
//  Created by Dave Murphy on 30/09/2016.
//  Copyright © 2016 www.silvercoinvaluer.com. All rights reserved.
//

import UIKit

class DBItem: NSObject {
    var country: String
    var name: String
    var years: String
    var pop: Int
    var fine: Double
    var asw: Double
    var tw: Double
    var notes: String
    var qty: Int
    var image1: String
    var image2: String
    var bookmark: Bool?
    let itemKey: String
    let userImage1: String
    let userImage2: String
    
    init(country: String, name: String, years: String, pop: Int, fine: Double, asw: Double, tw: Double, notes: String, qty: Int, image1: String, image2: String, bookmark: Bool?) {
        self.country = country
        self.name = name
        self.years = years
        self.pop = pop
        self.fine = fine
        self.asw = asw
        self.tw = tw
        self.notes = notes
        self.qty = qty
        self.image1 = image1
        self.image2 = image2
        self.bookmark = bookmark
        self.itemKey = UUID().uuidString
        self.userImage1 = UUID().uuidString
        self.userImage2 = UUID().uuidString
        
        super.init()
    }
    
    
    required init(coder aDecoder: NSCoder) {
        name = aDecoder.decodeObject(forKey: "name") as! String
        country = aDecoder.decodeObject(forKey: "country") as! String
        years = aDecoder.decodeObject(forKey: "years") as! String
        pop = aDecoder.decodeInteger(forKey: "pop") as Int
        fine = aDecoder.decodeDouble(forKey: "fine") as Double
        asw = aDecoder.decodeDouble(forKey: "asw") as Double
        tw = aDecoder.decodeDouble(forKey: "tw") as Double
        notes = aDecoder.decodeObject(forKey: "notes") as! String
        qty = aDecoder.decodeInteger(forKey: "qty") as Int
        image1 = aDecoder.decodeObject(forKey: "image1") as! String
        image2 = aDecoder.decodeObject(forKey: "image2") as! String
        bookmark = aDecoder.decodeBool(forKey: "bookmark") as Bool
        itemKey = aDecoder.decodeObject(forKey: "itemKey") as! String
        userImage1 = aDecoder.decodeObject(forKey: "userImage1") as! String
        userImage2 = aDecoder.decodeObject(forKey: "userImage2") as! String
        
        super.init()
    }
    
    
    func encodeWithCoder(_ aCoder: NSCoder) {
        aCoder.encode(country, forKey: "country")
        aCoder.encode(name, forKey: "name")
        aCoder.encode(years, forKey: "years")
        aCoder.encode(pop, forKey: "pop")
        aCoder.encode(fine, forKey: "fine")
        aCoder.encode(asw, forKey: "asw")
        aCoder.encode(tw, forKey: "tw")
        aCoder.encode(notes, forKey: "notes")
        aCoder.encode(qty, forKey: "qty")
        aCoder.encode(image1, forKey: "image1")
        aCoder.encode(image2, forKey: "image2")
        aCoder.encode(bookmark!, forKey: "bookmark")
        aCoder.encode(itemKey, forKey: "itemKey")
        aCoder.encode(userImage1, forKey: "userImage1")
        aCoder.encode(userImage2, forKey: "userImage2")
    }
}
