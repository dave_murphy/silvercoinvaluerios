//
//  DBItemsViewController.swift
//  SilverCoinValuerPRO
//
//  Created by Dave Murphy on 30/09/2016.
//  Copyright © 2016 www.silvercoinvaluer.com. All rights reserved.
//

import UIKit

@available(iOS 8.0, *)
class DBItemsViewController: UITableViewController {
    
    static let noBookmarks = "no_bookmarks"
    
    var itemStore: DBItemStore!
    var imageStore: ImageStore!
    var filteredDBItems = [DBItem]()
    var DBItems = [DBItem]()
    var currentDBItems = [DBItem]()
    var popularDBItems = [DBItem]()
    var totalBookmarks = [DBItem]()
    
    var words = [String]()
    var wordsSection = [String]()
    var wordsDict = [String:[DBItem]]()
    
    let searchController = UISearchController(searchResultsController: nil)

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        if searchController.isActive && searchController.searchBar.text != "" {
            return 1
        }
        return self.wordsDict.count
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive && searchController.searchBar.text != "" {
            return filteredDBItems.count
        }
        
        // find section title
        let sectionTitle = self.wordsSection[section]
        
        // find x list for given section title
        let x = self.wordsDict[sectionTitle]
        //print(x!)
        
        // return count for section
        return x!.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Get a new or recycled cell
        let cell = tableView.dequeueReusableCell(withIdentifier: "DBItemCell", for: indexPath) as! DBItemCell
        
        // Update the labels for the new preferred text size
        cell.updateLabels()
        
        // find section title
        let sectionTitle = self.wordsSection[indexPath.section]
        
        // find x list for given section title
        let x = self.wordsDict[sectionTitle]
        
        let dbItem: DBItem
        
        if searchController.isActive && searchController.searchBar.text != "" {
            dbItem = filteredDBItems[indexPath.row]
        } else {
            dbItem = x![indexPath.row]
        }
        
        cell.countryLabel.text = dbItem.country
        cell.nameLabel.text =  dbItem.name
        cell.yearsLabel.text = dbItem.years
        
        if dbItem.bookmark == true {
            cell.accessoryType = UITableViewCellAccessoryType.checkmark
            cell.tintColor = uicolorFromHex(rgbValue: 0x4bae32)
        }
        else {
            cell.accessoryType = UITableViewCellAccessoryType.none
        }
        return cell
    }

    
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        let prefs = UserDefaults.standard
        
        if(prefs.integer(forKey: SettingsViewController.selectedSearch) == 0) {
            // country search
            filteredDBItems = currentDBItems.filter { DBItem in
                return DBItem.country.lowercased().contains(searchText.lowercased())
            }
            
        } else if (prefs.integer(forKey: SettingsViewController.selectedSearch) == 1) {
            // name search
            filteredDBItems = currentDBItems.filter { DBItem in
                return DBItem.name.lowercased().contains(searchText.lowercased())
            }
        }
        tableView.reloadData()
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // If the triggered segue is the "ShowItem" segue
        if segue.identifier == "ShowItem" {
            let backItem = UIBarButtonItem()
            backItem.title = ""
            navigationItem.backBarButtonItem = backItem
            
            // Figure out which row was tapped
            // https://www.raywenderlich.com/113772/uisearchcontroller-tutorial
            if (tableView.indexPathForSelectedRow?.row) != nil {
                let dbItem: DBItem
                
                if searchController.isActive && searchController.searchBar.text != "" {
                    dbItem = filteredDBItems[(tableView.indexPathForSelectedRow?.row)!]
                } else {
                    let sectionTitle = self.wordsSection[(tableView.indexPathForSelectedRow?.section)!]
                    let x = self.wordsDict[sectionTitle]
                    dbItem = x![(tableView.indexPathForSelectedRow?.row)!]
                }
                
                let detailViewController = segue.destination as! DetailViewController
                detailViewController.item = dbItem
                detailViewController.imageStore = imageStore
            }
        }
        
        // If the triggered segue is the "ShowInfo" segue
        if segue.identifier == "showInfo" {
            let backItem = UIBarButtonItem()
            backItem.title = ""
            navigationItem.backBarButtonItem = backItem
        }
        
        // If the triggered segue is the "ShowSettings" segue
        if segue.identifier == "showSettings" {
            let backItem = UIBarButtonItem()
            backItem.title = ""
            navigationItem.backBarButtonItem = backItem
        }
    }
    
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if searchController.isActive && searchController.searchBar.text != "" {
            return "results"
        }
        return wordsSection[section]
    }
    
    
    override func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return wordsSection
    }
    
    
    override func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        return index
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        tableView.tableHeaderView = searchController.searchBar
        
        var titleView: UIImageView
        titleView = UIImageView(frame:CGRect(x: 0, y: 0, width: 25, height: 35))
        titleView.contentMode = .scaleAspectFit
        titleView.image = UIImage(named: "LOGO")
        self.navigationItem.titleView = titleView
        
        let settingsButton = UIButton()
        settingsButton.frame = CGRect(x: 0, y: 0, width: 31, height: 31)
        settingsButton.setImage(UIImage(named: "SETTINGS-ICON"), for: UIControlState())
        settingsButton.addTarget(self, action: #selector(DBItemsViewController.settingsButtonPressed), for: .touchUpInside)
        
        let infoButton = UIButton()
        infoButton.frame = CGRect(x: 0, y: 0, width: 31, height: 31)
        infoButton.setImage(UIImage(named: "info31"), for: UIControlState())
        infoButton.addTarget(self, action: #selector(DBItemsViewController.infoButtonPressed), for: .touchUpInside)
        
        let leftBarButton = UIBarButtonItem()
        leftBarButton.customView = settingsButton
        self.navigationItem.leftBarButtonItem = leftBarButton
        
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = infoButton
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 72
    }
    
    
    @objc func settingsButtonPressed() {
        print("Settings button pressed")
        performSegue(withIdentifier: "showSettings", sender: nil)
    }
    
    
    @objc func infoButtonPressed() {
        print("Info button pressed")
        performSegue(withIdentifier: "showInfo", sender: nil)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        currentDBItems.removeAll()
        //print("viewWillAppear1 + \(currentDBItems.count)")
        //tableView.reloadData()
        
        let prefs = UserDefaults.standard
        
        // set the search bar hint text
        if(prefs.integer(forKey: SettingsViewController.selectedSearch) == 0) {
            if(prefs.integer(forKey: SettingsViewController.selectedList) == 0) {
                searchController.searchBar.placeholder = "Search all coins by country"
            } else if (prefs.integer(forKey: SettingsViewController.selectedList) == 1) {
                searchController.searchBar.placeholder = "Search popular coins by country"
            } else if (prefs.integer(forKey: SettingsViewController.selectedList) == 2) {
                searchController.searchBar.placeholder = "Search coins ASW >2oz by country"
            } else if (prefs.integer(forKey: SettingsViewController.selectedList) == 3) {
                searchController.searchBar.placeholder = "Search bookmarked coins by country"
            }
        } else {
            if(prefs.integer(forKey: SettingsViewController.selectedList) == 0) {
                searchController.searchBar.placeholder = "Search all coins by name"
            } else if (prefs.integer(forKey: SettingsViewController.selectedList) == 1) {
                searchController.searchBar.placeholder = "Search popular coins by name"
            } else if (prefs.integer(forKey: SettingsViewController.selectedList) == 2) {
                searchController.searchBar.placeholder = "Search coins ASW >2oz by name"
            } else if (prefs.integer(forKey: SettingsViewController.selectedList) == 3) {
                searchController.searchBar.placeholder = "Search bookmarked coins by name"
            }
        }
        
        // Change title and currentDBItems depending on which coin list selected
        if(prefs.integer(forKey: SettingsViewController.selectedList) == 0) {
            //navigationItem.title = "All coins"
            currentDBItems = itemStore.allDBItems
        } else if (prefs.integer(forKey: SettingsViewController.selectedList) == 1) {
            //navigationItem.title = "Popular"
            for i in 0..<itemStore.allDBItems.count {
                if itemStore.allDBItems[i].pop == 1 {
                    currentDBItems.append(itemStore.allDBItems[i])
                }
            }
        } else if (prefs.integer(forKey: SettingsViewController.selectedList) == 2) {
            //navigationItem.title = "ASW >2oz"
            for i in 0..<itemStore.allDBItems.count {
                if itemStore.allDBItems[i].asw >= 2 {
                    currentDBItems.append(itemStore.allDBItems[i])
                }
            }
        } else if (prefs.integer(forKey: SettingsViewController.selectedList) == 3) {
            //navigationItem.title = "Bookmarks"
            for i in 0..<itemStore.allDBItems.count {
                if itemStore.allDBItems[i].bookmark! {
                    currentDBItems.append(itemStore.allDBItems[i])
                }
            }
        }
        
        // check if any bookmarks
        for i in 0..<itemStore.allDBItems.count {
            if itemStore.allDBItems[i].bookmark! {
                totalBookmarks.append(itemStore.allDBItems[i])
            }
        }
        if totalBookmarks.count == 0 {
            let prefs = UserDefaults.standard
            prefs.set(false, forKey: DBItemsViewController.noBookmarks)
            print("there is \(totalBookmarks.count) bookmarks")
        } else {
            let prefs = UserDefaults.standard
            prefs.set(true, forKey: DBItemsViewController.noBookmarks)
            print("there is \(totalBookmarks.count) bookmarks")
        }
        
        // generate the word dictionary for the index list
        generateWordsDict()
    }
    
    
    func generateWordsDict() {
        words.removeAll()
        wordsSection.removeAll()
        wordsDict.removeAll()
        
        // first generate the words string
        for i in 0..<currentDBItems.count {
            words.append(currentDBItems[i].country)
        }
        print(words)
        words = words.sorted()
        
        // set section title "" at initial
        var sectionTitle: String = ""
        
        // iterate all records from array
        for i in 0..<self.words.count {
            
            // get current record
            let currentRecord = self.words[i]
            
            // find first character from current record
            let firstChar = currentRecord[currentRecord.startIndex]
            
            // convert first character into string
            let firstCharString = "\(firstChar)"
            
            // if first character not match with past section title then create new section
            if firstCharString != sectionTitle {
                
                // set new title for section
                sectionTitle = firstCharString
                
                // add new section having key as section title and value as empty array of string
                self.wordsDict[sectionTitle] = [DBItem]()
                
                // append title within section title list
                self.wordsSection.append(sectionTitle)
            }
            
            // add record to the section
            self.wordsDict[firstCharString]?.append(currentDBItems[i])
        }
        //print(wordsSection)
        //print(wordsDict)
        tableView.reloadData()
    }
    
    
    func uicolorFromHex(rgbValue:UInt32)->UIColor{
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        
        return UIColor(red:red, green:green, blue:blue, alpha:1.0)
    }
}


extension DBItemsViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
    }
}
