//
//  DBItemStore.swift
//  SilverCoinValuerPRO
//
//  Created by Dave Murphy on 30/09/2016.
//  Copyright © 2016 www.silvercoinvaluer.com. All rights reserved.
//

import UIKit

class DBItemStore {
    
    var allDBItems = [DBItem]()
    let itemArchiveURL: URL = {
        let documentsDirectories = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectory = documentsDirectories.first!
        return documentDirectory.appendingPathComponent("items_v2_6_8.archive")
    }()
    
    
    init() {
        if let archivedItems = NSKeyedUnarchiver.unarchiveObject(withFile: itemArchiveURL.path) as? [DBItem] {
            //allDBItems.removeAll()
            allDBItems = archivedItems
        }
        else {
            // read in from silvercoins.csv
            let path = Bundle.main.path(forResource: "silvercoins", ofType: "csv")
            let fileMgr = FileManager.default
            
            if fileMgr.fileExists(atPath: path!) {
                
                do {
                    let fullText = try String(contentsOfFile: path!, encoding: String.Encoding.utf8)
                    let readings = fullText.components(separatedBy: "\n") as [String]
                    
                    for i in 1..<readings.count - 1 {
                        let coinsData = readings[i].components(separatedBy: ";")
                        
                        allDBItems.append(DBItem(country: coinsData[0], name: coinsData[1], years: coinsData[2], pop: Int(coinsData[3])!, fine: Double(coinsData[4])!, asw: Double(coinsData[5])!, tw: Double(coinsData[6])!, notes: coinsData[7], qty: Int(coinsData[8])!, image1: coinsData[9], image2: coinsData[10], bookmark: false))
                        
                    }
                } catch let error as NSError {
                    print("Error: \(error)")
                }
            }
        }
    }
    
    
    func saveChanges() -> Bool {
        print("Saving items to: \(itemArchiveURL.path)")
        return NSKeyedArchiver.archiveRootObject(allDBItems, toFile: itemArchiveURL.path)
    }
}
