//
//  InfoViewController.swift
//  SilverCoinValuerPRO
//
//  Created by Dave Murphy on 06/10/2016.
//  Copyright © 2016 www.silvercoinvaluer.com. All rights reserved.
//

import UIKit

class InfoViewController: UIViewController {
    
    @IBOutlet weak var buyingLabel: UILabel!
    @IBOutlet weak var howToLabel: UILabel!
    @IBOutlet weak var terminologyLabel: UILabel!
    @IBOutlet weak var numismaticLabel: UILabel!
    @IBOutlet weak var reliableLabel: UILabel!
    @IBOutlet weak var codesLabel: UILabel!
    @IBOutlet weak var websiteLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = ""
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        
        buyingLabel.text = "Silver Coin Valuer PRO is in early development. So, if there are new features or changes you’d like to see please get in touch. We'll try to include them in next update."
        
        howToLabel.text = "Go to Settings to get latest silver prices in USD, EUR, GBP or CAD. Here can also select pre-set coin lists, e.g. your bookmarked coins. Internet required to download images. They may take few seconds initially to download. Zoom in to see coin detail."
        
        terminologyLabel.text = "Actual Silver Weight (ASW) is the amount of silver in a coin measured in troy ounces (Toz). Total Weight (TW) of a coin is the combined weight in grams of its ASW plus any other metals present."
        
        numismaticLabel.text = "No effort is made to assign numismatic value. Use this app to determine silver content ('melt') value only. Coins are listed first alphabetically by country then in order of denomination, e.g. 1 Dollar, 2 Dollars etc."
        
        reliableLabel.text = "Without complete reliability and accuracy this app is of little value. We've spent hundreds of hours compiling the coin data with meticulous care and attention to detail. Data is drawn from dozens of sources. Years data is conservative, meaning specimens may exist outside the range shown. In many cases the photos do not represent all coins in the series."
        
        codesLabel.text = "C:  Years indicated are Christian calendar converted from non-Christian inscriptions on coin\nD:  Tricky to identify due to huge design variations\nG:  Gap years in indicated range\nK:  Some variants have gimmicky holograms, painted on designs, gemstones etc\nM:  Some or all years very limited mintage\nN:  Probably has numismatic value\nR:  Some examples are genuinely rare\nU:  Unusual shape(s)\nV:  Variations up to and exceeding 10% in ASW or fineness within same year and denomination. Be careful identifying\nX: Official versions exist that are partial or 100% non-silver. Be careful identifying"
        
        websiteLabel.text = "For advice including tax, storage and transportation issues related to buying, holding, and selling of physical silver (and gold) check out:"
    }
}
