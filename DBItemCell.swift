//
//  DBItemCell.swift
//  SilverCoinValuerPRO
//
//  Created by Dave Murphy on 30/09/2016.
//  Copyright © 2016 www.silvercoinvaluer.com. All rights reserved.
//

import UIKit

class DBItemCell: UITableViewCell {
    
    @IBOutlet var countryLabel: UILabel!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var yearsLabel: UILabel!
    
    
    func updateLabels() {
        let bodyFont = UIFont.preferredFont(forTextStyle: UIFontTextStyle.body)
        countryLabel.font = bodyFont
        nameLabel.font = bodyFont
        
        let caption1Font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.caption1)
        yearsLabel.font = caption1Font
    }
}
