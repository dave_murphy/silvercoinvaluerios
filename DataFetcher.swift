//
//  DataFetcher.swift
//  SilverCoinValuerPRO
//
//  Created by Dave Murphy on 25/10/2016.
//  Copyright © 2016-17 www.silvercoinvaluer.com. All rights reserved.
//

import Foundation

// Note: Dec 13 2016. This class not used now. fetchPrices() method subsumed into requestButton() method in SettingsViewController

class DataFetcher {
    
//    let session: NSURLSession = {
//        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
//        return NSURLSession(configuration: config)
//    }()
    
    
    func fetchPrices() {
        let request = URLRequest(url: URL(string: "http://www.goldandsilvertax.com/precious-metals.php")!)
        // note: could use the closure(?) above for 'session'
        // instead define it directly
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) -> Void in
            
            if let jsonData = data {
//                if let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) {
//                    print(jsonString)
//                }
            
                do {
                    let jsonObject: AnyObject = try JSONSerialization.jsonObject(with: jsonData, options: []) as! [String: Any] as AnyObject
                    //print(jsonObject)
                    let prefs = UserDefaults.standard
                    
                    if let usd = jsonObject["usd"] as? [String: Double] {
                        if var silverPrice = usd["silver"] {
                            silverPrice = silverPrice * 31.1
                            prefs.set(silverPrice, forKey: SettingsViewController.usdPrice)
                            print("Silver in USD: \(prefs.double(forKey: SettingsViewController.usdPrice))")
                        }
                    }
                    
                    if let eur = jsonObject["eur"] as? [String: Double] {
                        if var silverPrice = eur["silver"] {
                            silverPrice = silverPrice * 31.1
                            prefs.set(silverPrice, forKey: SettingsViewController.eurPrice)
                            print("Silver in EUR: \(prefs.double(forKey: SettingsViewController.eurPrice))")
                        }
                    }
                    
                    if let gbp = jsonObject["gbp"] as? [String: Double] {
                        if var silverPrice = gbp["silver"] {
                            silverPrice = silverPrice * 31.1
                            prefs.set(silverPrice, forKey: SettingsViewController.gbpPrice)
                            print("Silver in GBP: \(prefs.double(forKey: SettingsViewController.gbpPrice))")
                        }
                    }
                    
                    if let cad = jsonObject["cad"] as? [String: Double] {
                        if var silverPrice = cad["silver"] {
                            silverPrice = silverPrice * 31.1
                            prefs.set(silverPrice, forKey: SettingsViewController.cadPrice)
                            print("Silver in CAD: \(prefs.double(forKey: SettingsViewController.cadPrice))")
                        }
                    }
                }
                catch let error {
                    print("Error creating JSON object: \(error)")
                }
                
            }
            else if let requestError = error {
                print("Error fetching silver price data: \(requestError)")
            }
            else {
                print("Unexpected error with the request")
            }
        }) 
        task.resume()
        
    }
}
