//
//  DetailViewController.swift
//  SilverCoinValuerPRO
//
//  Created by Dave Murphy on 03/10/2016.
//  Copyright © 2016 www.silvercoinvaluer.com. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIScrollViewDelegate {
    
    static let whichImage = "which_image"
   
    @IBOutlet weak var imageViewSide1: UIImageView!
    @IBOutlet weak var imageViewSide2: UIImageView!
    @IBOutlet weak var countryField: UITextField!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var yearsField: UITextField!
    @IBOutlet weak var fineField: UITextField!
    @IBOutlet weak var ASWField: UITextField!
    @IBOutlet weak var TWField: UITextField!
    @IBOutlet weak var valueField: UITextField!
    @IBOutlet weak var notesView: UITextView!
    @IBOutlet weak var bookmarkSwitch: UISwitch!
    
    @IBOutlet weak var zoomView: UIView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    var item: DBItem! {
        didSet {
            navigationItem.title = item.country
        }
    }
    var imageStore: ImageStore!
    
    // use closure to instantiate the number formatter
    let numberFormatter: NumberFormatter = {
        let nf = NumberFormatter()
        nf.numberStyle = .decimal
        nf.minimumFractionDigits = 2
        nf.maximumFractionDigits = 2
        return nf
    }()
    
    
    @IBAction func takePicture(_ sender: UIButton) {
        // NOTE: Check for camera happens in useCamera() method
        let ac4 = UIAlertController(title: "Use Camera or Photo Library?", message: "", preferredStyle: .alert)
        
        let cameraAction = UIAlertAction(title: "Use Camera", style: .destructive, handler: { (action) -> Void in
            print("use camera")
            self.useCamera()
        })
        ac4.addAction(cameraAction)
        
        let libraryAction = UIAlertAction(title: "Use Photo Library", style: .destructive, handler: { (action) -> Void in
            print("use library")
            self.usePhotoLibrary()
        })
        ac4.addAction(libraryAction)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        ac4.addAction(cancelAction)
        
        // Present the alert controller
        present(ac4, animated: true, completion: nil)
    }
    
    
    func useCamera() {
        let ac2 = UIAlertController(title: "Replace which side?", message: "", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        ac2.addAction(cancelAction)
        
        let prefs = UserDefaults.standard
        
        let image1Action = UIAlertAction(title: "Front", style: .destructive, handler: { (action) -> Void in
            print("replace side 1 image with userImage1")
            prefs.set("image1", forKey: DetailViewController.whichImage)
            
            let imagePicker = UIImagePickerController()
            //check for camera, take a picture; otherwise pick from photo library
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePicker.sourceType = .camera
            }
            else {
            imagePicker.sourceType = .photoLibrary
            }
            imagePicker.delegate = self
            
            // Place image picker on the screen
            self.present(imagePicker, animated: true, completion: nil)
        })
        ac2.addAction(image1Action)
        
        let image2Action = UIAlertAction(title: "Reverse", style: .destructive, handler: { (action) -> Void in
            print("replace side 2 image with userImage2")
            prefs.set("image2", forKey: DetailViewController.whichImage)
            
            let imagePicker = UIImagePickerController()
            //check for camera, take a picture; otherwise pick from photo library
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePicker.sourceType = .camera
            }
            else {
                imagePicker.sourceType = .photoLibrary
            }
            imagePicker.delegate = self
            
            // Place image picker on the screen
            self.present(imagePicker, animated: true, completion: nil)
        })
        ac2.addAction(image2Action)
        
        // Present the alert controller
        present(ac2, animated: true, completion: nil)
    }
    
    
    func usePhotoLibrary() {
        let ac2 = UIAlertController(title: "Replace which side?", message: "", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        ac2.addAction(cancelAction)
        
        let prefs = UserDefaults.standard
        
        let image1Action = UIAlertAction(title: "Front", style: .destructive, handler: { (action) -> Void in
            print("replace side 1 image with userImage1")
            prefs.set("image1", forKey: DetailViewController.whichImage)
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .photoLibrary
            imagePicker.delegate = self
            
            // Place image picker on the screen
            self.present(imagePicker, animated: true, completion: nil)
        })
        ac2.addAction(image1Action)
        
        let image2Action = UIAlertAction(title: "Reverse", style: .destructive, handler: { (action) -> Void in
            print("replace side 2 image with userImage2")
            prefs.set("image2", forKey: DetailViewController.whichImage)
            
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .photoLibrary
            imagePicker.delegate = self
            
            // Place image picker on the screen
            self.present(imagePicker, animated: true, completion: nil)
        })
        ac2.addAction(image2Action)
        
        // Present the alert controller
        present(ac2, animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String: Any]) {
        // Get picked image from info dictionary
        let selectedImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        let prefs = UserDefaults.standard
        let imageString = prefs.object(forKey: DetailViewController.whichImage) as? String
        
        if (imageString?.contains("image1"))! {
            // Store the image in the ImageStore for the item's key
            imageStore.setImage(selectedImage, forKey:item.userImage1)
            // Put that image on the screen in the image view
            //imageViewSide1.image = selectedImage
        } else if (imageString?.contains("image2"))! {
            imageStore.setImage(selectedImage, forKey:item.userImage2)
            // Put that image on the screen in the image view
            //imageViewSide2.image = image
        }
        
        // Take image picker off the screen
        dismiss(animated: true, completion: nil)
    }
    
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return zoomView
    }
    
    
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        //TODO
    }
    
    
    @IBAction func restoreDefaultPhoto(_ sender: UIButton) {
        let title = "Restore both to default images?"
        
        let ac = UIAlertController(title: title, message: "", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        ac.addAction(cancelAction)
        
        let restoreAction = UIAlertAction(title: "Restore", style: .destructive, handler: { (action) -> Void in
            // Remove user generated images from Imagestore
            self.imageStore.deleteImageForKey(self.item.userImage1)
            self.imageStore.deleteImageForKey(self.item.userImage2)
            
            // Check if there's an image to download, if not put the 'no_image' image in ImageView
            let charset1 = self.item.image1
            if !charset1.contains("no_image") {
                print("download image1, display it, save to filesystem")
                print("http://silvercoinvaluer.com/scv_pro_images/\(self.item.image1)/.png")
                self.downloadImage1(urlString: "http://silvercoinvaluer.com/scv_pro_images/\(self.item.image1).png")
            } else {
                print("show default image1")
                self.imageViewSide1.image = UIImage(named: self.item.image1)
            }
            
            let charset2 = self.item.image2
            if !charset2.contains("no_image") {
                print("download image2, display it, save to filesystem")
                print("http://silvercoinvaluer.com/scv_pro_images/\(self.item.image2)/.png")
                self.downloadImage2(urlString: "http://silvercoinvaluer.com/scv_pro_images/\(self.item.image2).png")
            } else {
                print("show default image2")
                self.imageViewSide2.image = UIImage(named: "no_image")
                self.imageViewSide2.image = UIImage(named: self.item.image2)
            }
        })
        
        ac.addAction(restoreAction)
        
        // Present the alert controller
        present(ac, animated: true, completion: nil)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.scrollView.delegate = self
        
        var titleView : UIImageView
        // set the dimensions
        titleView = UIImageView(frame:CGRect(x: 0, y: 0, width: 25, height: 35))
        // Set the aspect
        titleView.contentMode = .scaleAspectFit
        titleView.image = UIImage(named: "LOGO")
        
        countryField.text = item.country
        nameField.text = item.name
        yearsField.text = item.years
        fineField.text = String(item.fine)
        ASWField.text = String(item.asw)
        TWField.text = String(item.tw)
        
        //silver content value in correct currency: ASW x price
        let prefs = UserDefaults.standard
        
        if(prefs.integer(forKey: SettingsViewController.selectedCurrency)) == 0 {
            // use USD
            let price = item.asw * prefs.double(forKey: SettingsViewController.usdPrice)
            if (price < 1000) {
                numberFormatter.maximumFractionDigits = 2;
            } else {
                numberFormatter.maximumFractionDigits = 0;
            }
            valueField.text = "USD " + numberFormatter.string(from: NSNumber(value: price))!
        } else if (prefs.integer(forKey: SettingsViewController.selectedCurrency)) == 1 {
            // use EUR
            let price = item.asw * prefs.double(forKey: SettingsViewController.eurPrice)
            if (price < 1000) {
                numberFormatter.maximumFractionDigits = 2;
            } else {
                numberFormatter.maximumFractionDigits = 0;
            }
            valueField.text = "EUR " + numberFormatter.string(from: NSNumber(value: price))!
        } else if (prefs.integer(forKey: SettingsViewController.selectedCurrency)) == 2 {
            // use GBP
            let price = item.asw * prefs.double(forKey: SettingsViewController.gbpPrice)
            if (price < 1000) {
                numberFormatter.maximumFractionDigits = 2;
            } else {
                numberFormatter.maximumFractionDigits = 0;
            }
            valueField.text = "GBP " + numberFormatter.string(from: NSNumber(value: price))!
        } else if (prefs.integer(forKey: SettingsViewController.selectedCurrency)) == 3 {
            // use CAD
            let price = item.asw * prefs.double(forKey: SettingsViewController.cadPrice)
            if (price < 1000) {
                numberFormatter.maximumFractionDigits = 2;
            } else {
                numberFormatter.maximumFractionDigits = 0;
            }
            valueField.text = "CAD " + numberFormatter.string(from: NSNumber(value: price))!
        }
        
        //TODO - make notes can be fully scrolled through
        notesView.text = item.notes
        
        // unwrap safely
        if (item.bookmark) != nil {
            bookmarkSwitch.setOn(item.bookmark!, animated: true)
            bookmarkSwitch.onTintColor = uicolorFromHex(rgbValue: 0x4BAE32)
        }
        else {
            bookmarkSwitch.setOn(false, animated: true)
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let charset1 = item.image1
        // if there is a filesystem image associated with item display it
        if let imageToDisplay1 = imageStore.imageForKey(item.image1) {
            print("image1 from filesystem has name and key \(item.image1)")
            imageViewSide1.image = imageToDisplay1
        } else if !charset1.contains("no_image") {
            print("download image1, display it, save to filesystem with key of same name")
            downloadImage1(urlString: "http://silvercoinvaluer.com/scv_pro_images/\(item.image1).png")
        } else {
            imageViewSide1.image = UIImage(named: item.image1)
        }
        
        let charset2 = item.image2
        // if there is a filesystem image associated with item display it
        if let imageToDisplay2 = imageStore.imageForKey(item.image2) {
            print("image2 from filesystem has name and key \(item.image2)")
            imageViewSide2.image = imageToDisplay2
        } else if !charset2.contains("no_image") {
            print("download image2, display it, save to filesystem with key of same name")
            downloadImage2(urlString: "http://silvercoinvaluer.com/scv_pro_images/\(item.image2).png")
        } else {
            print("show default image")
            self.imageViewSide2.image = UIImage(named: "no_image")
            imageViewSide2.image = UIImage(named: item.image2)
        }
        
        // now - check if user created images, if so put these in image views
        if let imageToDisplay3 = imageStore.imageForKey(item.userImage1) {
            print("detected userImage1. Stick that in image1 for now")
            imageViewSide1.image = imageToDisplay3
        }
        if let imageToDisplay4 = imageStore.imageForKey(item.userImage2) {
            print("detected userImage2")
            imageViewSide2.image = imageToDisplay4
        }
    }
    
    
    func downloadImage1(urlString: String) {
        URLSession.shared.dataTask(with: NSURL(string: urlString)! as URL, completionHandler: { (data1, response, error) -> Void in
            
            if error != nil {
                print(error ?? "Image download failed")
                return
            }
            
            DispatchQueue.main.async(execute: { () -> Void in
                let image1 = UIImage(data: data1!)
                self.imageStore.setImage(image1!, forKey:self.item.image1)
                self.imageViewSide1.image = image1
                print("imageViewSide1 is http://silvercoinvaluer.com/scv_pro_images/\(self.item.image1).png")
                print("DispatchQueue() method called")
            })
            
        }).resume()
    }
    
    
    func downloadImage2(urlString: String) {
        URLSession.shared.dataTask(with: NSURL(string: urlString)! as URL, completionHandler: { (data2, response, error) -> Void in
            
            if error != nil {
                print(error ?? "Image download failed")
                return
            }
            
            DispatchQueue.main.async(execute: { () -> Void in
                let image2 = UIImage(data: data2!)
                
                // unwrap safely. This is in case there's image1 but not image2
                if (image2) != nil {
                    self.imageStore.setImage(image2!, forKey:self.item.image2)
                    self.imageViewSide2.image = image2
                    print("imageViewSide2 is http://silvercoinvaluer.com/scv_pro_images/\(self.item.image2).png")
                }
                else {
                    print("no image2")
                }
                
                print("DispatchQueue() method called")
            })
            
        }).resume()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = ""
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // "Save" bookmark and image changes to DBItem
        if bookmarkSwitch.isOn {
            item.bookmark = true
        } else {
            item.bookmark = false
        }
        
        //print("bookmarkSwitch at ViewWillDisappear is \(String(describing: item.bookmark))")
    }
    
    
    func uicolorFromHex(rgbValue:UInt32)->UIColor{
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        
        return UIColor(red:red, green:green, blue:blue, alpha:1.0)
    }
}
